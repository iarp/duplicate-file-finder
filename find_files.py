import os
import hashlib
import csv
from collections import defaultdict

# Which folder to scan?
directory = 'U:/Sailing/'


############################


def hashfile(afile, hasher, blocksize=65536):
    """ Some file could be larger than available RAM. Break up reading into segments and update hasher """
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher


hashes = defaultdict(list)
counter = 0
for root, subFolders, files in os.walk(directory):
    for filename in files:
        counter += 1

        if filename.endswith('.url'):
            continue

        file_loc = os.path.join(root, filename)

        # Calculate the files hash
        hash = hashfile(open(file_loc, 'rb'), hashlib.md5()).hexdigest()

        hashes[hash].append(file_loc)

        # nothing more than visual display that something is happening.
        if counter % 100 == 0:
            print(counter, file_loc, hash)


with open('matches.csv', 'w', newline='') as csv_matches:
    csv_file = csv.writer(csv_matches, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

    for hash, matched in hashes.items():
        if len(matched) > 1:
            csv_file.writerow([hash]+matched)

with open('matches_2.csv', 'w', newline='') as csv_matches:
    csv_file = csv.writer(csv_matches, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

    for hash, matched in hashes.items():
        if len(matched) > 1:
            csv_file.writerow([hash])
            for match in matched:
                csv_file.writerow(['',match])
