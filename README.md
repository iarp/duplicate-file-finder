Dupicate File Finder
====================

This script will search a single directory for images that are identical

Update the ``directory = '''`` value and run it with Python 3

It will output 2 csv files for excel. The both contain the same information
it's only the presentation of that information that differs between the files.

matches.csv
-----------

First column contains the hash, all other columns on the same row contain the
 match file paths.
 
This file is useful for other programming needs.

matches_2.csv
-------------

This file lists the hash in the first column, then the rows below it until
you reach the next hash are all files that match the hash above the file path.

This one is easiest for manually reading the paths.
